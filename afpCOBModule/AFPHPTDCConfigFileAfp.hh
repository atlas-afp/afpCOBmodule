#ifndef AFPHPTDCCONFIGFILE_HH
#define AFPHPTDCCONFIGFILE_HH

#include "AFPHPTDCModuleConfig.hh"
#include <string>
#include <map>
#include <vector>
#include "variant.hpp"

class AFPHPTDCConfigFile {
public:
  AFPHPTDCConfigFile(){}
  ~AFPHPTDCConfigFile();
  void readModuleConfig(ipc::AFPHPTDCModuleConfig* cfg, std::string filename);
  void writeModuleConfig(ipc::AFPHPTDCModuleConfig* cfg, const std::string &base, const std::string &confdir, const std::string &configname, const std::string &key);
  void writeCalibFile(float cal[2][12][ipc::IPC_N_CALIBVALS] , const std::string filename, int, int);
  void toVariant(const ipc::AFPHPTDCModuleConfig &cfg, variant32 &v);
  void dump(const ipc::AFPHPTDCModuleConfig *cfg);
  void testlink() {};
private:
  std::string getFullPath(std::string relPath);
  void setupCalib(float cal[2][12][ipc::IPC_N_CALIBVALS] , std::string par, int i1, int i2);
  unsigned lookupToUnsigned(std::string par, int index, int size=0);
  int convertToUnsigned(std::string par, unsigned & val, int size);
  //float lookupToFloat(std::string par);
  void dumpCalib(const float cal[2][12][ipc::IPC_N_CALIBVALS], int i1, int i2);

  std::string m_moduleCfgFilePath;
  std::ifstream *m_moduleCfgFile;
  std::map<std::string, std::vector<std::string> > m_params;
};

#endif
