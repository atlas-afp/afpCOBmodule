#ifndef HITBUSCONFIG_HH
#define HITBUSCONFIG_HH

#include "config/PixelConfig.hh"
#include "HitbusModuleConfig.hh"
#include "HitbusConfigFileAfp.hh"

#include <variant.hpp>

class HitbusConfig: public PixelConfig{
public:
  HitbusConfig(std::string filename);
  HitbusConfig(const variant32 &vconf);
  virtual ~HitbusConfig(){
    delete m_config;
  }

  virtual int downloadConfig(int rce, int id);
  virtual void* getStruct(){return (void*)m_config;}
  virtual void writeModuleConfig(const std::string &base, const std::string &confdir, const std::string &configname, const std::string &key);

private:
  ipc::HitbusModuleConfig *m_config;
};

#endif
