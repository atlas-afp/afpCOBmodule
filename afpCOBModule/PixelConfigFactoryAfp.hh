#ifndef PIXELCONFIGFACTORY_HH
#define PIXELCONFIGFACTORY_HH

#include <variant.hpp>

class PixelConfig;

class PixelConfigFactory{
public:
  PixelConfig* createConfig(const char*, const variant32 &vconf);
};

#endif
