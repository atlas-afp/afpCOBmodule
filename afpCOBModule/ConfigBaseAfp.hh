#ifndef CONFIGBASE_HH
#define CONFIGBASE_HH

#include "PixelConfigAfp.hh"
#include "PixelConfigFactoryAfp.hh"

#include <variant.hpp>

class ConfigBase{
public:
  ConfigBase(const char* name, int inlink, int outlink, int rce, int phase, const char* filename, const variant32 &vconf): 
      m_cfg(0), m_inlink(inlink), m_outlink(outlink), m_rce(rce), m_phase(phase), m_name(name), m_vconf(vconf){
    PixelConfigFactory pf;
    m_cfg=pf.createConfig(filename, m_vconf);
  }
  ~ConfigBase(){
    delete m_cfg;
  }
  
  PixelConfig* getModuleConfig(){
    return m_cfg;
  }
  
  int getInlink(){
    return m_inlink;
  }
  int getOutlink(){
    return m_outlink;
  }
  int getRce(){
    return m_rce;
  }
  int getPhase(){
    return m_phase;
  }

private:
  PixelConfig* m_cfg;
  int m_inlink;
  int m_outlink;
  int m_rce;
  int m_phase;
  std::string m_name;
  const variant32 &m_vconf;
};

#endif
