#ifndef _AFP_COOL_HPP
#define _AFP_COOL_HPP

#include <iostream>
#include <sstream>
#include <locale>
#include <iomanip>
#include "CoolVariant/TimeBasedCoolVariant.hh"
#include <variant.hpp>
#include <filesystem>
#include <config/FEI4/FEI4BConfigFile.hh>
#include <config/hitbus/HitbusConfigFile.hh>
#include <config/afp-hptdc/AFPHPTDCConfigFile.hh>

std::string  CoolFolderRoot = "/AFP/Onl/Config";
class AfpReadCool {
public:
    AfpReadCool(const std::string &conn, const std::string tag, time_t ts, variant32 &v) :
            m_conn(conn), m_tag(tag), m_ts(ts), m_v(v) {}
        void read() {
        _readCool("SideA", "Far", "all");
        _readCool("SideA", "Near", "all");
        _readCool("SideC", "Far", "all");
        _readCool("SideC", "Near", "all");
    }

        void readSiT() {
        _readCool("SideA", "Far", "SiT");
        _readCool("SideA", "Near", "SiT");
        _readCool("SideC", "Far", "SiT");
        _readCool("SideC", "Near", "SiT");
    }
        void readSiTA() {
        _readCool("SideA", "Far", "SiT");
        _readCool("SideA", "Near", "SiT");
    }
        void readSiTC() {
        _readCool("SideC", "Far", "SiT");
        _readCool("SideC", "Near", "SiT");
    }
        void readSiTAN() {_readCool("SideA", "Near", "SiT");}
        void readSiTAF() {_readCool("SideA", "Far", "SiT");}
        void readSiTCN() {_readCool("SideC", "Near", "SiT");}
        void readSiTCF() {_readCool("SideC", "Far", "SiT");}
        
        void readToF() {
        _readCool("SideA", "Far", "ToF");
        _readCool("SideC", "Far", "ToF");
    }
        void readToFA() {_readCool("SideA", "Far", "ToF");}
        void readToFC() {_readCool("SideC", "Far", "ToF");}

        void readRCE() {
        _readCool("SideA", "Near", "rce");
        _readCool("SideC", "Near", "rce");
    }
        void readRCEA() {_readCool("SideA", "Near", "rce");}
        void readRCEC() {_readCool("SideC", "Near", "rce");}
        //_readCool("SideA", "Near", "rce");

private:
    void _readCool(const std::string &side, const std::string &dist, const std::string &type) {
        variant32 v;
       if (type == "all" or type == "SiT") {
           try {
              std::string folder = CoolFolderRoot + "/" + side + "/" + dist + "/SiT";
              std::string tag = "Afp" + side + dist + "SiT-" + m_tag;
              TimeBasedCoolVariantReader<variant32> cool(m_conn, folder, tag);
              cool.read(v, m_ts);
          } catch (...) {}
          m_v[side][dist]["SiT"]=v;
       }
       if (type == "all" or type == "ToF") {
             try {
                std::string folder = CoolFolderRoot + "/" + side + "/" + dist + "/ToF";
                std::string tag = "Afp" + side + dist + "ToF-" +  m_tag;
                TimeBasedCoolVariantReader<variant32> cool(m_conn, folder, tag);
                cool.read(v, m_ts);
             } catch (...) {}
             m_v[side][dist]["ToF"]=v;
       }
       if (type == "all" or type == "rce") {
             try {
                std::string folder = CoolFolderRoot + "/" + side + "/RCE";
                std::string tag = "Afp" + side + "RCE-" +  m_tag;
                TimeBasedCoolVariantReader<variant32> cool(m_conn, folder, tag);
                cool.read(v, m_ts);
             } catch (...) {}
             m_v[side][dist]["rce"]=v;
       }

    }
private:
    std::string m_conn;
    std::string m_tag;
    time_t m_ts;
    variant32 &m_v;

};
class AfpWriteCool {
public:
    AfpWriteCool(const std::string &conn, const std::string tag, time_t ts, const variant32 &v) :
            m_conn(conn), m_tag(tag), m_ts(ts), m_v(v) {}

    void write() {
        _writeCool("SideA", "Near");
        _writeCool("SideA", "Far");
        _writeCool("SideC", "Near");
        _writeCool("SideC", "Far");
    }

    void writeSiT() {
        _writeCoolSiT("SideA", "Near");
        _writeCoolSiT("SideA", "Far");
        _writeCoolSiT("SideC", "Near");
        _writeCoolSiT("SideC", "Far");
    }

    void writeToF() {
        _writeCoolToF("SideA", "Far");
        _writeCoolToF("SideC", "Far");
    }

    void writeRCE() {
        _writeCoolRCE("SideA", "Near");
        _writeCoolRCE("SideC", "Near");
    }

private:
    void _writeCoolSiT(const std::string &side, const std::string &dist) {
        const variant32 &v = m_v[side][dist]["SiT"];
        const variant32 &hb = v["hitbus"];
        const variant32 &m =  v["modules"];

        std::string folder = CoolFolderRoot + "/" + side + "/" + dist + "/SiT";
        std::string tag = "Afp" + side + dist + "SiT-" + m_tag;
        TimeBasedCoolVariantWriter<variant32> cool(m_conn, folder, tag);
        cool.write(v, m_ts);

        //folder = CoolFolderRoot + "/" + side + "/" + dist + "/SiT/Planes";
        //tag = "Afp" + side + dist + "SiTPlanes-" + m_tag;
        //TimeBasedCoolVariantWriter<variant32> cool1(m_conn, folder, tag);
        //cool1.write(m, m_ts);
    }

    void _writeCoolToF(const std::string &side, const std::string &dist) {
        const variant32 &v = m_v[side][dist]["ToF"];
        const variant32 &t =  v["tdc"];
               std::string folder = CoolFolderRoot + "/" + side + "/" + dist + "/ToF";
               std::string tag = "Afp" + side + dist + "ToF-" + m_tag;
               TimeBasedCoolVariantWriter<variant32> cool(m_conn, folder, tag);
               cool.write(v, m_ts);
    }

    void _writeCoolRCE(const std::string &side, const std::string &dist) {
           const variant32 &v = m_v[side][dist];
           const variant32 &r =  v["rce"];
                  std::string folder = CoolFolderRoot + "/" + side +  "/RCE";
                  std::string tag = "Afp" + side + "RCE-" + m_tag;
                  TimeBasedCoolVariantWriter<variant32> cool(m_conn, folder, tag);
                  cool.write(r, m_ts);
    }


    void _writeCool(const std::string &side, const std::string &dist) {
        const variant32 &v = m_v[side][dist];
        const variant32 &hb = v["hitbus"];

        if (hb["module"] == "hitbus") {
            std::string folder = CoolFolderRoot + "/" + side + "/" + dist + "/Hitbus";
            std::string tag = "Afp" + side + dist + "Hitbus-" + m_tag;
            TimeBasedCoolVariantWriter<variant32> cool(m_conn, folder, tag);
            cool.write(hb, m_ts);
        }
        unsigned count = 0;
        // this is what we want but CoolUtils doesn't provide yet
        // one folder for all module configs with four channels
        if (dist == "Far") {
               std::string folder = CoolFolderRoot + "/" + side + "/" + dist + "/ToFTDC";
               std::string tag = "Afp" + side + dist + "ToFTDC-" + m_tag;
               TimeBasedCoolVariantWriter<variant32> cool(m_conn, folder, tag);
               for (auto const &m : v["tdc"]) {
                   if (!m["module"].is_null()) {
                    cool.write(m, m_ts,count);
                   }
                   count++;
               }
       }

       count = 0;

        std::string folder = CoolFolderRoot + "/" + side + "/" + dist + "/SiTPlanes";
        std::string tag = "Afp" + side + dist + "SiTPlanes-" + m_tag;
        TimeBasedCoolVariantWriter<variant32> cool(m_conn, folder, tag);
        // workaround for now:  four folders, four tags

        for (auto const &m : v["modules"]) {
            if (!m["module"].is_null()) {
                //workraround
                //std::string folder = CoolFolderRoot + "/" + side + "/" + dist + "/P"+std::to_string(count);
                //std::string tag = "Afp" + side + dist + "P"+std::to_string(count)+  "-" + m_tag;
                //TimeBasedCoolVariantWriter<variant32> cool(m_conn, folder, tag);
                cool.write(m, m_ts,count);
            }
            count++;
        }

    }

    std::string m_conn;
    std::string m_tag;
    time_t m_ts;
    const variant32 &m_v;
};



class AfpConfigReader {
public:
    AfpConfigReader(const std::string &aside,const std::string &cside,variant32 &v) :
    m_aside(aside),m_cside(cside),m_v(v) {}
    void read() {
        variant32 a,c;
        _readConfig(m_aside,a);
        _readConfig(m_cside,c);
        m_v["SideA"]=a;
        m_v["SideC"]=c;
    }
private:

    void _readConfig(const std::string &name,variant32 &v) {
  std::ifstream ifs;
  try {
    ifs.open(name);
  }
  catch(...) {
    std::cout << "Failed to read: " << name << std::endl;
    exit(-1);
  }
  variant32 near,far;
  variant32 hitbus_near;
  variant32 hitbus_far;
  variant32 tdc;
  for(unsigned i=0;i<4;i++)
    near.push_back(_readEntry(ifs));
  hitbus_near=_readEntry(ifs);
  for(unsigned i=0;i<4;i++)
    far.push_back(_readEntry(ifs));
  hitbus_far=_readEntry(ifs);
  for(unsigned i=0;i<2;i++)
    tdc.push_back(_readEntry(ifs));
  v["Far"]["SiT"]["modules"]=far;
  v["Far"]["SiT"]["hitbus"]=hitbus_far;
  v["Far"]["ToF"]["tdc"]=tdc;
  v["Near"]["SiT"]["modules"]=near;
  v["Near"]["SiT"]["hitbus"]=hitbus_near;
}
    variant32 _readEntry(std::ifstream &ifs) {
  variant32 v;
  std::string fn;
  bool is_hitbus=false;
  bool is_tdc=false;
  uint32_t enabled,inlink,outlink,rce,phase;
  ifs>>fn;
  ifs>>enabled;
  ifs>>inlink;
  ifs>>outlink;
  ifs>>rce;
  ifs>>phase;
  //v["enabled"]=bool(enabled);
  v["inlink"]=inlink;
  v["outlink"]=outlink;
  v["rce"]=rce;

  if (fn.find("hitbus") != std::string::npos) is_hitbus=true;
  if (fn.find("hptdc") != std::string::npos) is_tdc=true;
  std::vector<std::string> dirname;
  for(auto& dpart : std::filesystem::path(fn)) dirname.push_back(dpart);
  //v["module"]="hitbus";
  if(is_hitbus) {
    v["module"]="hitbus";
    variant32 mod;
    ipc::HitbusModuleConfig config;
    HitbusConfigFile f;
    f.readModuleConfig(&config,fn);
    f.toVariant(config,mod);
    v["config"]=mod;
  } else if(is_tdc) {
      v["module"]="hptdc";
      variant32 mod;
      ipc::AFPHPTDCModuleConfig config;
      AFPHPTDCConfigFile f;
      f.readModuleConfig(&config,fn);
      f.toVariant(config,mod);
      v["config"]=mod;
  } else {
    if(fn!="None" && dirname.size()>=2 ) {
      v["module"]="FEI4B";
      variant32 mod;
      mod["moduleID"] = std::string(dirname[dirname.size() - 3]);
      ipc::PixelFEI4BConfig config;
      FEI4BConfigFile f;
      f.readModuleConfig(&config,fn);
      f.toVariant(config,mod);
      v["config"]=mod;
    } else  v["module"] = nullptr;
  }
  return v;
}
     std::string m_aside;
     std::string m_cside;
     variant32 &m_v;
};
#endif
