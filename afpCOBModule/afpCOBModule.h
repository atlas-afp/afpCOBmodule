#ifndef AFPCOBMODULE_H
#define AFPCOBMODULE_H

#include <string>
#include <vector>
#include <iostream>

#include "ROSCore/ReadoutModule.h"
#include "DFSubSystemItem/Config.h"

#include "ipc/core.h"
#include "ipc/partition.h"
#include "RCDVme/RCDVme.h"

#include "is/info.h"
#include "oh/OHRawProvider.h"
#include "RCDrunControlAfp.hh"

#include <variant.hpp>

class AbsController;

namespace RCD {

  using namespace ROS;
  using namespace RCD;

  class afpCOBModule : public ReadoutModule
  {
   public:

    /** The constructor should have no arguments */
    afpCOBModule ();
    virtual ~afpCOBModule () noexcept;

    // Methods for the FSM state changes
    virtual void setup (DFCountedPointer<Config> configuration);

    virtual void configure      (const daq::rc::TransitionCmd &);
    virtual void connect        (const daq::rc::TransitionCmd &) {};
    virtual void prepareForRun  (const daq::rc::TransitionCmd &);
    virtual void stopDC         (const daq::rc::TransitionCmd &);
    virtual void disconnect     (const daq::rc::TransitionCmd &);
    virtual void unconfigure    (const daq::rc::TransitionCmd &);

    virtual void publish         ();
    virtual void publishFullStats();
    virtual void resynch        (const daq::rc::ResynchCmd &cmd);
    virtual void user           (const daq::rc::UserCmd &cmd) {};

    virtual void enable         (const std::vector <std::string> &args) {};
    virtual void disable        (const std::vector <std::string> &args);

    virtual void onExit         (daq::rc::FSM_STATE) noexcept;

    virtual const std::vector<DataChannel *> *channels ();
    virtual void clearInfo () ;
    virtual DFCountedPointer<Config> getInfo () ;

    void resetHisto (std::string histoName);

   private:


    int m_status;
    int m_triggerLatency;
    int m_effectiveLatency;

    afpCOBModule* m_afpCOBModule;
    RCDrunControl* m_RCDrunControl;
    RCF::RcfInitDeinit rcfInit;

    // for IS
    IPCPartition m_ipcpartition;
    daq::core::Partition* m_partition;
    Configuration* m_confDB;
    DFCountedPointer<Config> m_configuration;
    AbsController* m_controller;


    // configuration parameters
    std::string m_UID;
    std::string m_IS_server;
    std::string m_IS_conf_server;

    std::string m_side;

    variant32 m_configCool;

    bool m_useCool;
    bool m_useConfiguration;
    bool m_useMonitoring;
    bool m_doPublish;

  };

  inline const std::vector<DataChannel *> *afpCOBModule::channels ()
  {
    return 0;
  }
}


#endif 
