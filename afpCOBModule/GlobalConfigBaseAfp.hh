#ifndef GLOBAL_CONFIG_BASE_HH
#define GLOBAL_CONFIG_BASE_HH

#include "ConfigBaseAfp.hh"
#include <vector>
#include <variant.hpp>

typedef std::vector<ConfigBase*>::iterator configIterator;

class GlobalConfigBase{
public:
  enum constants{MAX_MODULES=64};
  GlobalConfigBase(const char* confdir, const char* filename, const variant32& vconf);
  ~GlobalConfigBase();
  configIterator begin(){return m_configs.begin();}
  configIterator end(){return m_configs.end();}
  unsigned int size(){return m_configs.size();}  
private: 
  std::vector<ConfigBase*> m_configs;
};

#endif
