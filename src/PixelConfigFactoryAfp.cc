#include "PixelConfigFactoryAfp.hh"
#include "PixelConfigAfp.hh"
#include "config/FEI4/FEI4AConfig.hh"
#include "FEI4BConfigAfp.hh"
//#include "config/FEI3/ModuleConfig.hh"
#include "HitbusConfigAfp.hh"
#include "config/afp-hptdc/AFPHPTDCConfig.hh"
#include <regex>
#include <sys/stat.h> 
#include "util/exceptions.hh"
#include <iostream>
#include <variant.hpp>

PixelConfig* PixelConfigFactory::createConfig(const char* filename, const variant32 &vconf){
  struct stat stFileInfo;
  int intStat;

    std::string inpline;
    std::cout << "PixelConfigFactory - vconf " <<std::endl;
    if (vconf.empty()) {
  // Attempt to get the file attributes 
      intStat = stat(filename, &stFileInfo);
      if(std::string(filename)=="None" || intStat != 0) { //File does not exist
          std::cout<<"File "<<filename<<" does not exist."<<std::endl;
           rcecalib::Config_File_Error err;
           throw err;
      } else {
        std::ifstream cfgFile(filename);
        getline(cfgFile, inpline); //read the first line to determine configuration format
        cfgFile.close();
      }
    } else {
        inpline=vconf["module"];
        std::cout << " PixelConfigFactory::createConfig: inpline: " <<inpline << std::endl;
    }
    std::regex r1("TurboDAQ");
    std::regex r2("FEI4A");
    std::regex r3("FEI4B");
    std::regex r4("Hitbus");
    std::regex r5("Hptdc");
    if(std::regex_search(inpline, r1)==true){
      return 0; // new ModuleConfig(filename);
    }else if(std::regex_search(inpline, r2)==true){ 
      return new FEI4AConfig(filename);
    }else if(std::regex_search(inpline, r3)==true){ 
      //return new FEI4BConfig(filename);
      std::cout << "selected FEI4B from regex"<<std::endl;
      return new FEI4BConfig(vconf);
    } else if(std::regex_search(inpline, r4)==true){
      //return new HitbusConfig(filename);
      std::cout << "selected Hitbus from regex"<<std::endl;
      return new HitbusConfig(vconf);
    } else if(std::regex_search(inpline, r5)==true){
      return new AFPHPTDCConfig(filename);
    } else {
      std::cout<<"Unknown file type."<<std::endl;
      rcecalib::Config_File_Error err;
      throw err;
    }
  return 0;
}

