#include "HitbusConfigAfp.hh"
#include <iostream>

#include <RCF/RCF.hpp>
#include "RCFHitbusAdapter.hh"
#include "util/RceName.hh"

#include <variant.hpp>

int HitbusConfig::downloadConfig(int rce, int id){
  char binding[32];
  char rcename[32];
  sprintf(binding, "I_RCFHitbusAdapter_%d", id);
  //sprintf(rcename, RCFHOST"%d", rce);
  sprintf(rcename, "ACM-AFP-ROD-0%d", rce);
  try {
    RcfClient<I_RCFHitbusAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT), binding);
    client.RCFdownloadConfig( *m_config );
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
}

HitbusConfig::HitbusConfig(std::string filename): PixelConfig("Hitbus", filename, false, 0, 0, 0), 
				      m_config(new ipc::HitbusModuleConfig){
    HitbusConfigFile turbo;
    turbo.readModuleConfig(m_config,filename);
    m_name=(const char*)m_config->idStr;
    m_id=strtol((const char*)m_config->idStr,0,10);
    m_valid=true;
  }

HitbusConfig::HitbusConfig(const variant32 &vconf): 
    PixelConfig("Hitbus", std::string("tojestlipa2"), false, 0, 0, 0), 
    m_config(new ipc::HitbusModuleConfig){
    HitbusConfigFile turbo;
      std::cout << "HitbusConfig::HitbusConfig vconf " << std::endl;
      turbo.fromVariant(vconf["config"], *m_config);
    m_name=(const char*)m_config->idStr;
    m_id=strtol((const char*)m_config->idStr,0,10);
    m_valid=true;
  }

void HitbusConfig::writeModuleConfig(const std::string &base, const std::string &confdir, const std::string &configname, const std::string &key){
    HitbusConfigFile cf;
    cf.writeModuleConfig(m_config, base, confdir, configname, key);
  }
