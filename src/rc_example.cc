#include <iostream>
#include <iomanip>
#include <cmdl/cmdargs.h>
#include <variant.hpp>
#include "afpCOBModule/AfpCool.hpp"

int main(int argc,char *argv[])
{

  CmdArgStr   timestamp ('u', "timestamp", "timestamp", "timestamp");
  CmdArgStr   dbConn('d',"dbConn","database-connection","string",CmdArg::isVALREQ );
  CmdArgStr   dbTag('t',"dbTag","database-tag","string",CmdArg::isVALREQ);
  CmdLine  cmd(*argv, &timestamp,&dbConn,&dbTag,NULL);
  CmdArgvIter  arg_iter(--argc, ++argv);
  cmd.set(CmdLine::NO_ABORT|CmdLine::QUIET);
  cmd.parse(arg_iter);
  time_t ts;
if(timestamp) {
      std::tm t = {};
      std::istringstream ss((const char *) timestamp);
      ss >> std::get_time(&t, "%Y-%m-%d %H:%M:%S");
      if (ss.fail()) {
          std::cerr << "Parsing timestamp failed\n";
          return -1;
      } else {
          std::cout << std::put_time(&t, "%c") << '\n';
      }
      ts=std::mktime(&t);
  } else ts=std::time(NULL);
    variant32 v;
  try {
    std::string conn= (const char*)dbConn;
    std::string tag=  (const char*)dbTag;
    AfpReadCool cool(conn,tag,ts,v);
    cool.readSiT();
    cool.readToF();
    cool.readRCE();

    //cool.read();
  } catch(...) {
    std::cerr << "Cool read failed\n"; return -1;
  }
    for (const auto &side  : std::vector<std::string>({"SideA", "SideC"})) {
        for (const auto &dist  : std::vector<std::string>({"Near", "Far"})) {
            const variant32 &j = v[side][dist];

            if (!j["SiT"]["hitbus"].is_null()) {
                const variant32 &hb = j["SiT"]["hitbus"]["config"];
                //ipc::HitbusModuleConfig config;
                //HitbusConfigFile f;
                //f.fromVariant(hb,config);
                std::cout << "**** Found Hitbus: Side: " << side << ", Dist: " << dist << std::endl;
                std::cout << " function_A: " << hb["function_A"] << " inlink: " <<j["SiT"]["hitbus"]["inlink"] << " moduleId: " <<hb["moduleID"] << std::endl;
                //f.dump(config);
            }
            if (dist == "Far") { 
                if (!j["ToF"]["hptdc"].is_null()) {
                    std::cout << " found hptdc" <<std::endl;
                    for (unsigned i = 0; i < 2; i++) {
                        const variant32 &tdc = j["ToF"]["hptdc"][i];
                        if(tdc.is_null()) continue;
                        std::cout << "**** Found HPTDC: Side: " << side << ", Dist: " << dist << std::endl;
                        const variant32 &n = tdc["config"];
                        std::cout << "inlink: " << tdc["inlink"] << " moduleID: " << n["tdc_global"]["ModuleID"] << std::endl;
                    }
                }
            }
            if (!j["SiT"]["modules"].is_null()) {
                const variant32 &m = j["SiT"]["modules"];
                for (unsigned i = 0; i < 4; i++) {
                    const variant32 &mc = m[i];
                    if(mc.is_null()) continue;
                    //ipc::PixelFEI4BConfig config;
                    //FEI4BConfigFile f;
                    //f.fromVariant(mc["config"], config);
                    std::cout << "**** Found Module: Side: " << side << ", Dist: " << dist << std::endl;
                    std::cout << " inlink: " << mc["inlink"] << " moduleId: " << mc["config"]["moduleID"] << std::endl;
                    //f.dump(config);
                }
            }
            if ((dist == "Near") && (side == "SideA")){ 
                if (!j["rce"].is_null()) {
                    const variant32 &r = j["rce"]["afpCOBModule"];
                    std::cout << " found rce" <<std::endl;
                    std::cout << " globalDir: " << j["rce"]["afpCOBModule"]["afpRCDCOB-VLDB-A"]["GlobalConfigName"] << std::endl;
                }
            }
    }
  }
  return 0;
}
