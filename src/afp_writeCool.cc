#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>
#include <cmdl/cmdargs.h>
#include <variant.hpp>
#include "afpCOBModule/AfpCool.hpp"

int main(int argc,char *argv[])
{
 
  CmdArgStr   timestamp ('u', "timestamp", "timestamp", "timestamp");
  CmdArgStr   dbConn('d',"dbConn","database-connection","string",CmdArg::isVALREQ );
  CmdArgStr   dbTag('t',"dbTag","database-tag","string",CmdArg::isVALREQ);
  CmdArgStr   aSideConfig('a',"aSide","config-file","filename",CmdArg::isVALREQ);
  CmdArgStr   cSideConfig('c',"cSide","config-file","filename",CmdArg::isVALREQ);

  CmdLine  cmd(*argv, &timestamp,&dbConn,&dbTag,&aSideConfig,&cSideConfig,NULL);
  CmdArgvIter  arg_iter(--argc, ++argv);
  cmd.set(CmdLine::NO_ABORT|CmdLine::QUIET);
  cmd.parse(arg_iter);
  time_t ts;
if(timestamp) {
      std::tm t = {};
      std::istringstream ss((const char *) timestamp);
      ss >> std::get_time(&t, "%Y-%m-%d %H:%M:%S");
      if (ss.fail()) {
          std::cerr << "Parsing timestamp failed\n";
          return -1;
      } else {
          std::cout << std::put_time(&t, "%c") << '\n';
      }
      ts=std::mktime(&t);
  } else ts=std::time(NULL);


variant32 v;
AfpConfigReader r((const char*) aSideConfig,(const char*) cSideConfig,v);
r.read();
//v.dump();
  std::ifstream ifs;
  try {
    ifs.open("aSide_rce.json");
  }
  catch(...) {
    std::cout << "Failed to read: aSice_rce.json" << std::endl;
    exit(-1);
  }

variant32 vrce;
variant32::parse(ifs,vrce);
std::cout << " after json parser" << std::endl;
vrce.dump();

v["SideA"]["Near"]["rce"]=vrce;

//variant32& rceA = v["SideA"]["Near"]; 
//rceA["rce"] = vrce;
std::cout << " after joining rce data" << std::endl;
v.dump();

std::string conn= (const char*)dbConn;
std::string tag=  (const char*)dbTag;
  try {
  AfpWriteCool cool(conn,tag,ts,v);
  //cool.write();
  cool.writeSiT();
  cool.writeToF();
  //cool.writeRCE();
  } catch(...) {
    std::cerr << "Cool write failed\n"; return -1;
  }
  return 0;
}
