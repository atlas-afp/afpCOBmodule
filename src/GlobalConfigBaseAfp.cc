#include "GlobalConfigBaseAfp.hh"
#include "util/exceptions.hh"
#include <fstream>
#include <iostream>
#include <sys/stat.h> 
#include <variant.hpp>

GlobalConfigBase::GlobalConfigBase(const char* confdir, const char* filename, const variant32& vconf){

  if (!vconf.empty()) {
       std::string mfn="COOL";
       int inlink, outlink, rce, phase;
       bool included;
       char modname[128];
       // load FEI4B
       for(int i=0;i<8;i++){
       const variant32 &mf = i<4 ? vconf["Far"]["SiT"]["modules"] : vconf["Near"]["SiT"]["modules"];
         sprintf(modname, "Module_%d", i);
         inlink = mf[i%4]["inlink"];
         outlink = mf[i%4]["outlink"];
         rce = mf[i%4]["rce"];
         phase = 0;

         if (!mf[i%4]["module"].empty()) {
             mfn = std::string(mf[i%4]["config"]["moduleID"]);
             std::string mfn_corr = "0" + mfn.substr(mfn.find_first_of("_")+2,1) + mfn.substr(mfn.find_last_of("_")+2);
             std::cout << "modname FEI4B: " << modname << " inlink: " << inlink << " outlink: " << outlink << " rce: " << rce << " mfn_corr: " << mfn_corr << std::endl; 
	     m_configs.push_back(new ConfigBase(modname, inlink, outlink, rce, phase, mfn_corr.c_str(),mf[i%4]));
         }
       }
       //hitbus chips for near and far stations
       for(int i=0;i<2;i++){
       const variant32 &mf = i<1 ? vconf["Far"]["SiT"]["hitbus"] : vconf["Near"]["SiT"]["hitbus"];
         sprintf(modname, "Module_%d", 8+i);
         inlink = mf["inlink"];
         outlink = mf["outlink"];
         rce = mf["rce"];
         phase = 0;
         if (!mf["module"].empty()){
             mfn = std::string(mf["config"]["moduleID"]);
             std::cout << "modname hitbus: " << modname << " inlink: " << inlink << " outlink: " << outlink << " rce: " << rce << " mfn: " << mfn << std::endl; 
	     //m_configs.push_back(new ConfigBase(modname, inlink, outlink, rce, phase, mfn.c_str(),mf));
         }
       }
  }
  else {
     std::string fn=std::string(confdir)+"/"+filename;
     struct stat stFileInfo;
     int intStat;
     intStat = stat(fn.c_str(), &stFileInfo);
     if(fn=="None" || intStat != 0) { //File does not exist
       std::cout<<"File "<<fn<<" does not exist."<<std::endl;
       rcecalib::Config_File_Error err;
       throw err;
     }else{
       std::ifstream ifile(fn.c_str());
       std::string mfn;
       int inlink, outlink, rce, phase;
       bool included;
       char modname[128];
       for(int i=0;i<MAX_MODULES;i++){
         sprintf(modname, "Module_%d", i);
         ifile>>mfn;
         ifile>>included;
         ifile>>inlink;
         ifile>>outlink;
         ifile>>rce;
         ifile>>phase;
         mfn = std::string(confdir)+"/"+mfn;
         if(included==true){
	   m_configs.push_back(new ConfigBase(modname, inlink, outlink, rce, phase, mfn.c_str(), vconf));
         }
       }
     }
   }
}

GlobalConfigBase::~GlobalConfigBase(){
  for(configIterator it=begin(); it!=end(); it++){
    delete *it;
  }
}
