#include <fstream>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <stdlib.h>
//#include <time.h>
#include <list>
#include <ctime>
#include <algorithm>

#include <fstream>
#include <string>
#include <vector>


#include "config/Configuration.h"

#include "config/Configuration.h"
#include "dal/Resource.h"
#include "dal/Detector.h"
#include "dal/Partition.h"
#include "dal/util.h"
#include "dal/TriggerConfiguration.h"
#include "dal/OnlineSegment.h"

#include "afpCOBModule/afpCOBModule.h"
#include "afpCOBModule/AfpCool.hpp"

#include "ers/ers.h"

#include "ipc/partition.h"
#include <is/infodictionary.h>
#include "is/info.h"

#include "oh/OHRawProvider.h"

#include "server/RCFController.hh"
#include "RCDrunControlAfp.hh"

#include <config/ConfigObject.h>
#include <config/Configuration.h>

#include <RunControl/Common/UserExceptions.h>

#include <time.h>
#include <variant.hpp>


namespace RCD 
{
  using namespace RCD;
  using namespace ROS;

  /******************************************************/
  afpCOBModule::afpCOBModule() :
    m_triggerLatency(-1),
    m_effectiveLatency(-1),
    m_ipcpartition(getenv("TDAQ_PARTITION")),
    m_partition(NULL),
    m_confDB(NULL),
    m_configuration(),
    m_UID(""),
    m_IS_server(""),
    m_doPublish(false)
    /******************************************************/
  {
    std::string sout = m_UID + "@afpCOBModule::afpCOBModule() ";
    ERS_DEBUG(1,sout << "Entered");

    //RCF::init();
    //my_command_listener.setProvider ( this );
    ERS_DEBUG(1, sout << "Done");
  }
  
  /******************************************************/
  afpCOBModule::~afpCOBModule() noexcept
  /******************************************************/
  {
    std::string sout = m_UID + "@afpCOBModule::~afpCOBModule() ";
    ERS_DEBUG(1, sout << "Entered");
    delete m_controller;
    delete m_RCDrunControl;
    ERS_DEBUG(1, sout << "Done");
  }

  /******************************************************************/
  void afpCOBModule::setup(DFCountedPointer<Config> configuration)
  /******************************************************************/
  {
    std::string sout = m_UID + "@afpCOBModule::setup() ";
    ERS_LOG(sout << "Entered");

    //m_UID = configuration->getString("UID");
    m_UID = "afpCOBModule";
    if (m_UID == "") {
      ERS_LOG(sout << "Name parameter is \"\", setting to default: \"afpCOBModule_without_name\"");
      m_UID = "afpCOBModule_without_name";
    }
    sout = m_UID + "@afpCOBModule::setup() ";

    try {
      m_confDB = configuration->getPointer<Configuration>("configurationDB");
    } catch (CORBA::SystemException& ex) {
      ERS_LOG ("Creating configrationDB error : CORBA::SystemException ex._name()= ");
      //ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );

    } catch (std::exception& ex) {
      ERS_LOG ( "Creating configrationDB error : std::exception ex.what()=");
      //ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );
    }
   
    ERS_LOG(sout << "UID = " << m_UID);

    bool m_useCool = configuration->getBool("useCOOL");
    ERS_LOG(sout << "useCOOL = " << m_useCool);

    if (m_useCool) {
         m_side = configuration->getString("side");
         ERS_LOG(sout << "side = " << m_side);
         std::string sideLetter = (m_side.find("A") == std::string::npos) ? "C" : "A";
         std::cout << "side string: "<<m_side << std::endl;
         std::string tempStr = "afpRCDCOB-VLDB-"+sideLetter;
         std::cout << "tempStr: " <<tempStr.c_str();
     
         if (m_useCool){ 
               time_t ts = std::time(NULL);
               //std::string conn= "'sqlite://;schema=/det/afp/releases/testCOOL/afpdaq/mydb.db;dbname=TESTDB'"; //(const char*)dbConn;
               std::string conn= "sqlite_file:/det/afp/releases/testCOOL/afpdaq/mydb.db/TESTDB"; //(const char*)dbConn;
               std::string tag= "Run3-UPD1-00-00"; // (const char*)dbTag;
               AfpReadCool cool(conn,tag,ts,m_configCool);
               (m_side.find("A") == std::string::npos) ? cool.readSiTC() : cool.readSiTA();
         } 
    //const variant32 &vrr = v[side]["Near"]["rce"]["afpCobModule"];
         const variant32 &vrr = m_configCool[m_side]["Near"]["SiT"];
         const variant32 &hbv = vrr["hitbus"];
         std::cout << " hitbus dump: "<< std::endl;
         int inlink, outlink, rce;
         inlink = hbv["inlink"];
         outlink = hbv["outlink"];
         int rceNb = hbv["rce"];
         std::cout << "inlink: "  << inlink << " outlink: " << outlink << " rce: " <<rceNb << std::endl;
         const variant32 &fev = vrr["modules"];
         for (int index=0; index<4; index++) {
            int inlink, outlink, rce;
            inlink = fev[index]["inlink"];
            outlink = fev[index]["outlink"];
            rce = fev[index]["rce"];
            std::cout << "inlink: "  << inlink << " outlink: " << outlink << " rce: " <<rce << std::endl;
         }
    } else {
      m_side = "NotCool";
    }


    m_IS_server = configuration->getString("IS_server");
    ERS_LOG(sout << "m_IS_server = " << m_IS_server);
    m_IS_conf_server = "RunParams";


    m_controller = new RCFController;

    m_RCDrunControl = new RCDrunControl(*m_controller, m_ipcpartition, m_side);

    std::string aString = configuration->getString("GlobalConfigDir"); //useCOOL ? std::string(vr["GlobalConfigDir"]): configuration->getString("GlobalConfigDir");
    char *cstr = &aString[0];
    m_RCDrunControl->SetGlobalConfDir(cstr);
    ERS_LOG(sout << "GlobalConfigDir: " << aString);

    if (m_useCool) {
       aString = "/det/afp/databases/test.json";
    } else {
       aString = configuration->getString("GlobalConfigName"); 
    }
    cstr = &aString[0];
    m_RCDrunControl->SetGlobalConfName(cstr);
    ERS_LOG(sout << "GlobalConfigName: " << aString);

    int aLatency = configuration->getInt("chipLatency");
    m_RCDrunControl->SetLatency(aLatency); // 10 for ATLAS combined
    ERS_LOG(sout << "chipLatency: " << aLatency);

    int aConsecTriggers = configuration->getInt("ConsecTriggers");
    m_RCDrunControl->SetConsecLvl1TrigA(aConsecTriggers); // Readout window, 1 for ATLAS combined
    ERS_LOG(sout << "ConsecTriggers: " << aConsecTriggers);

    int aTriggerMask = configuration->getInt("TriggerMask");
    m_RCDrunControl->SetTriggerMask(aTriggerMask); // 32: TTC ; 2: Cyclic triggers
    ERS_LOG(sout << "TriggerMask: " << aTriggerMask);

    int aEventInterval = configuration->getInt("EventInterval");
    m_RCDrunControl->SetEventInterval(aEventInterval);
    ERS_LOG(sout << "EventInterval: " << aEventInterval);

    int aDeadTime = configuration->getInt("DeadTime");
    m_RCDrunControl->SetDeadTime(aDeadTime); // simple deadtime
    ERS_LOG(sout << "DeadTime: " << aDeadTime);

    int aRodId = configuration->getInt("RodId");
    m_RCDrunControl->SetRodId(aRodId); // simple deadtime
    ERS_LOG(sout << "RodId: " << std::hex<< aRodId <<std::dec);

    //m_RCDrunControl->SetStrobeLVL1Delay(100); // trigger delay, 100 for ATLAS combined, Standalone: 101
    m_triggerLatency = configuration->getInt("triggerLatency");
    ERS_LOG(sout << "triggerLatency from OKS: " << m_triggerLatency);
    int aDelta = configuration->getInt("DeltaTimeForLatency");
    ERS_LOG(sout << "DeltaTimeForLatency: " << aDelta);
    bool aSetLongLatency = configuration->getBool("SetLongLatency");
    ERS_LOG(sout << "SetLongLatency: " << aSetLongLatency);
    if (aSetLongLatency) {
       m_effectiveLatency = m_triggerLatency - aDelta; 
    } else {
       m_effectiveLatency = m_triggerLatency;
    }
    ERS_LOG(sout << "effective triggerLatency: " << m_triggerLatency);
    m_RCDrunControl->SetStrobeLVL1Delay(m_effectiveLatency);
    
    bool aEnableComplexDeadtime = configuration->getBool("EnableComplexDeadtime");
    m_RCDrunControl->SetEnableComplexDeadTime(aEnableComplexDeadtime); // enable/disable complex deadtime
    ERS_LOG(sout << "EnableComplexDeadtime: " << aEnableComplexDeadtime);

    int aComplexDeadtimeWindow = configuration->getInt("ComplexDeadtimeWindow");
    m_RCDrunControl->SetComplexDeadTimeWindow(aComplexDeadtimeWindow); // 15 ComplexDeadTimeWindow
    ERS_LOG(sout << "ComplexDeadtimeWindow: " << aComplexDeadtimeWindow);

    int aComplexDeadtimeBuffer = configuration->getInt("ComplexDeadtimeBuffer");
    m_RCDrunControl->SetComplexDeadTimeBuffer(aComplexDeadtimeBuffer); // 350 ComplexDeadtimeBuffer
    ERS_LOG(sout << "ComplexDeadtimeBuffer: " << aComplexDeadtimeBuffer);
    
    int aEventNormalization = configuration->getInt("EventNormalization");
    m_RCDrunControl->SetEventNormalization(aEventNormalization); // Update every 1000 events
    ERS_LOG(sout << "EventNormalization: " << aEventNormalization);

    bool aEnableSLinkBlowoff = configuration->getBool("SLinkBlowoff");
    m_RCDrunControl->SetEnableSLinkBlowoff(aEnableSLinkBlowoff); // Update every 1000 events
    ERS_LOG(sout << "EnableSLinkBlowoff: " << aEnableSLinkBlowoff);

    bool aEnableMonitoring = configuration->getBool("EnableMonitoring");
    m_RCDrunControl->SetEnableMonitoring(aEnableMonitoring); // Update every 1000 events
    ERS_LOG(sout << "EnableMonitoring: " << aEnableMonitoring);

    int aEfbTimeout = configuration->getInt("EfbTimeout");
    m_RCDrunControl->SetEfbTimeout(aEfbTimeout); // Update every 1000 events
    ERS_LOG(sout << "EfbTimeout: " << aEfbTimeout);

    int aEfbTimeoutFirst = configuration->getInt("EfbTimeoutFirst");
    m_RCDrunControl->SetEfbTimeoutFirst(aEfbTimeoutFirst); // Update every 1000 events
    ERS_LOG(sout << "EfbTimeoutFirst: " << aEfbTimeoutFirst);

    int aMissingHeaderTimeout = configuration->getInt("MissingHeaderTimeout");
    m_RCDrunControl->SetEfbMissingHeaderTimeout(aMissingHeaderTimeout); // Update every 1000 events
    ERS_LOG(sout << "MissingHeaderTimeout: " << aMissingHeaderTimeout);

    bool aEnableTtcSim = configuration->getBool("EnableTtcSim");
    m_RCDrunControl->SetEnableTtcSim(aEnableTtcSim); // Update every 1000 events
    ERS_LOG(sout << "EnableTtcSim: " << aEnableTtcSim);

    bool aBlockEcrBcr = configuration->getBool("BlockEcrBcr");
    m_RCDrunControl->SetBlockEcrBcr(aBlockEcrBcr); // Update every 1000 events
    ERS_LOG(sout << "BlockEcrBcr: " << aBlockEcrBcr);

    bool aOutputBusy = configuration->getBool("OutputBusy");
    m_RCDrunControl->SetOutputBusy(aOutputBusy); // Update every 1000 events
    ERS_LOG(sout << "OutputBusy: " << aOutputBusy);

    bool aDisableInternalBusy = configuration->getBool("DisableInternalBusy");
    m_RCDrunControl->SetDisableInternalBusy(aDisableInternalBusy); // Update every 1000 events
    ERS_LOG(sout << "DisableInternalBusy: " << aDisableInternalBusy);

    bool aEnableEcrReset = configuration->getBool("EnableEcrReset");
    m_RCDrunControl->SetEnableEcrReset(aEnableEcrReset); // Update every 1000 events
    ERS_LOG(sout << "EnableEcrReset: " << aEnableEcrReset);

    bool aEnableBcrBusy = configuration->getBool("EnableBcrBusy");
    m_RCDrunControl->SetEnableBcrBusy(aEnableBcrBusy); // Update every 1000 events
    ERS_LOG(sout << "EnableBcrBusy: " << aEnableBcrBusy);

    int aBcidOffset = configuration->getInt("BcidOffset");
    m_RCDrunControl->SetBcidOffset(aBcidOffset); // Update every 1000 events
    ERS_LOG(sout << "BcidOffset: " << aBcidOffset);

    int aBcrVetoFirstBC = configuration->getInt("BcrVetoFirstBC");
    m_RCDrunControl->SetBcrVetoFirstBC(aBcrVetoFirstBC); // Update every 1000 events
    ERS_LOG(sout << "BcrVetoFirstBC: " << aBcrVetoFirstBC);

    int aBcrVetoWidth = configuration->getInt("BcrVetoWidth"); 
    m_RCDrunControl->SetBcrVetoWidth(aBcrVetoWidth); // Update every 1000 events
    ERS_LOG(sout << "BcrVetoWidth: " << aBcrVetoWidth);

    bool aEnableTimingScan = configuration->getBool("EnableTimingScan"); 
    m_RCDrunControl->SetTimingScan(aEnableTimingScan); // Update every 1000 events
    ERS_LOG(sout << "EnableTimingScan: " << aEnableTimingScan);

    int aEventSizeLimit = configuration->getInt("FrontendEventSizeLimit"); 
    m_RCDrunControl->SetEventSizeLimit(aEventSizeLimit); // Update every 1000 events
    ERS_LOG(sout << "EventSizeLimit: " << aEventSizeLimit);

    int aClockDelay1 = configuration->getInt("ClockDelayNSAP0P3");
    ERS_LOG(sout << "ClockDelayNSAP0P3: " << aClockDelay1);

    int aClockDelay2 = configuration->getInt("ClockDelayNSAP1");
    ERS_LOG(sout << "ClockDelayNSAP1: " << aClockDelay2);

    int aClockDelay3 = configuration->getInt("ClockDelayNSAP2");
    ERS_LOG(sout << "ClockDelayNSAP2: " << aClockDelay3);

    int aClockDelay4 = configuration->getInt("ClockDelayNSAHB");
    ERS_LOG(sout << "ClockDelayNSAHB: " << aClockDelay4);

    int aClockDelay5 = configuration->getInt("ClockDelayFSAP0P3");
    ERS_LOG(sout << "ClockDelayFSAP0P3: " << aClockDelay5);

    int aClockDelay6 = configuration->getInt("ClockDelayFSAP1TDC1");
    ERS_LOG(sout << "ClockDelayFSAP1TDC1: " << aClockDelay6);

    int aClockDelay7 = configuration->getInt("ClockDelayFSAP2TDC2");
    ERS_LOG(sout << "ClockDelayFSAP2TDC2: " << aClockDelay7);

    int aClockDelay8 = configuration->getInt("ClockDelayFSAHB");
    ERS_LOG(sout << "ClockDelayFSAHB: " << aClockDelay8);

    int aClockDelay11 = configuration->getInt("ClockDelayNSCP0P3");
    ERS_LOG(sout << "ClockDelayNSCP0P3: " << aClockDelay11);

    int aClockDelay12 = configuration->getInt("ClockDelayNSCP1");
    ERS_LOG(sout << "ClockDelayNSCP1: " << aClockDelay12);

    int aClockDelay13 = configuration->getInt("ClockDelayNSCP2");
    ERS_LOG(sout << "ClockDelayNSCP2: " << aClockDelay13);

    int aClockDelay14 = configuration->getInt("ClockDelayNSCHB");
    ERS_LOG(sout << "ClockDelayNSCHB: " << aClockDelay14);

    int aClockDelay15 = configuration->getInt("ClockDelayFSCP0P3");
    ERS_LOG(sout << "ClockDelayFSCP0P3: " << aClockDelay15);

    int aClockDelay16 = configuration->getInt("ClockDelayFSCP1TDC1");
    ERS_LOG(sout << "ClockDelayFSCP1TDC1: " << aClockDelay16);

    int aClockDelay17 = configuration->getInt("ClockDelayFSCP2TDC2");
    ERS_LOG(sout << "ClockDelayFSCP2TDC2: " << aClockDelay17);

    int aClockDelay18 = configuration->getInt("ClockDelayFSCHB");
    ERS_LOG(sout << "ClockDelayFSCHB: " << aClockDelay18);

    //m_RCDrunControl-> SetBcrBusyParams(3543,20);

    ERS_LOG(sout << "Done");
}

  /******************************************************/
  void afpCOBModule::configure(const daq::rc::TransitionCmd&)
  /******************************************************/
  {
    std::string sout = m_UID + "@afpCOBModule::configure() ";
    ERS_LOG(sout << "Entered");

    const std::string dict_entry = m_IS_server + "." + m_UID ;
    const std::string dict_entryConfig = m_IS_server + "." + m_UID + "Config";
    ERS_LOG( sout << dict_entry.c_str() << "; " << m_ipcpartition.name());

    if(!m_RCDrunControl->Init()){
          std::string errorText = "No configuration loaded";
          std::string errTxt = m_useCool ? errorText + " from COOl" : errorText + " from file";
          ERS_LOG(sout << errTxt);
          ERS_REPORT_IMPL( ers::fatal, ers::Message, errTxt, );
    }
    //if(!(m_RCDrunControl->isAtlasClockPresent())){
      //ERS_LOG(sout << "ATLAS clock not present");
      //ERS_REPORT_IMPL( ers::fatal, ers::Message, "ATLAS clock not present.", );
    //}
    //if(!(m_RCDrunControl->isExternalClockSet())){ 
      //ERS_LOG(sout << "External clock not set");
      //ERS_REPORT_IMPL( ers::fatal, ers::Message, "External clock not set.", );
    //}

    if(!(m_RCDrunControl->isGbtStatusOK())){ 
      ERS_LOG(sout << "GBT Status not OK.");
      ERS_REPORT_IMPL( ers::fatal, ers::Message, "GBT Status not OK.", );
    }

    m_RCDrunControl->Config();

    ERS_LOG(sout << "latencyCheck");

    std::string data;
    std::string name = getenv("TDAQ_PARTITION");
    int latencyValue = 0;
    try
    {
      Configuration db(data);

      // find partition and register variables converter
      if (const daq::core::Partition * p = daq::core::get_partition(db, name))
        {
          const daq::core::TriggerConfiguration* trigConf=p->get_TriggerConfiguration();
          if (trigConf != 0) {
             latencyValue = trigConf->get_LatencyValue();
             ERS_LOG ( sout << "partition latency value = "<<latencyValue);
          } else {
             ERS_LOG ( sout << "partition trigger configuration not found");
          }
       }
     else
       {
           ERS_LOG (sout << "ERROR: cannot find partition " << name) ;
       }
     }
     catch (ers::Issue & ex)
     {
        ERS_LOG( sout << "ERROR: caught " << ex);
     }

     if (latencyValue != (m_triggerLatency - m_effectiveLatency)) { 
          std::string message1 = "latency loaded: ";
          std::string afpLatMsg =  std::to_string(m_triggerLatency);
          std::string message2 =  " - partitionLatency: ";
          std::string globalLatMsg = std::to_string(latencyValue);
          std::string message = "";
          message.append(message1);
          message.append(afpLatMsg);
          message.append(message2);
          message.append(globalLatMsg);
          daq::rc::LatencyMismatch afpLatencyProblem(ERS_HERE, "AFP", message);
          ers::error(afpLatencyProblem);
     }
      ERS_LOG(sout << "Done");
  }

  /**********************************************************/
  void afpCOBModule::unconfigure(const daq::rc::TransitionCmd&)
  /**********************************************************/
 {
   std::string sout = m_UID + "@afpCOBModule::unconfigure() ";
   ERS_LOG(sout << "Entered");
   m_RCDrunControl->UnConfig();
   ERS_LOG(sout << "Done");
 }

  /**********************************************************/
  void afpCOBModule::prepareForRun  (const daq::rc::TransitionCmd &) 
  /**********************************************************/
  {
    std::string sout = m_UID + "@afpCOBModule::prepareForRun() ";
    ERS_LOG(sout << "Entered");
    if (m_runParams != 0) {
       int arunNumber = m_runParams->getInt( "runNumber" );
       ERS_LOG(sout << "RunNumber: "<<arunNumber);
       m_RCDrunControl-> SetRunNumber(arunNumber);
    } else {
       ERS_LOG(sout << "probems with setting RunNumber: - will be set to 0xabcdef");
       m_RCDrunControl-> SetRunNumber(0xabcdef);
    }
    m_RCDrunControl->ResetL1id();
    m_RCDrunControl->StartRun();
    m_doPublish=true;
    ERS_LOG(sout << "Done");
  }

  /**********************************************************/
  void afpCOBModule::stopDC  (const daq::rc::TransitionCmd &) 
  /**********************************************************/
  {
    std::string sout = m_UID + "@afpCOBModule::stopDC() ";
    ERS_LOG(sout << "Entered");
    m_doPublish=false;
    m_RCDrunControl->StopRun();
    ERS_LOG(sout << "Done");
  }

  /**********************************************************/
  void afpCOBModule::disconnect     (const daq::rc::TransitionCmd &)
  /**********************************************************/
  {
    std::string sout = m_UID + "@afpCOBModule::disconnect() ";
    ERS_LOG(sout << "Entered");
    //    m_RCDrunControl->StopRun();
    ERS_LOG(sout << "Done");
  }
  /******************************************************/
  DFCountedPointer<Config> afpCOBModule::getInfo()
  /******************************************************/
  {
    std::string sout = m_UID + "@afpCOBModule::getInfo() ";
    ERS_LOG(sout << "Entered");

    DFCountedPointer<Config> configuration = Config::New();
  
    ERS_LOG(sout << "Done");
    return configuration;
  }

  /******************************************************/
  void afpCOBModule::clearInfo()
  /******************************************************/
  {
    std::string sout = m_UID + "@afpCOBModule::clearInfo() ";
    ERS_LOG(sout << "Entered");
    ERS_LOG(sout << "Done");
  }

  /******************************************************/
  void afpCOBModule::publish()
  /******************************************************/
  {
    std::string sout = m_UID + "@afpCOBModule::publish() ";
    ERS_DEBUG(1, sout << "Entered");
    try{
      if(m_doPublish==true)m_RCDrunControl->publish( ); 
    }catch(...){
      ERS_LOG(sout << "Publishing failed.");
      ERS_REPORT_IMPL( ers::warning, ers::Message, "Publishing failed.", );
    }
    ERS_DEBUG(1, sout << "Done");
  }
 
  /******************************************************/
  void afpCOBModule::publishFullStats()
  /******************************************************/
  {  
    std::string sout = m_UID + "@afpCOBModule::publishFullStats() ";
    ERS_DEBUG(1, sout << "Entered");

    // publish counter value
    this->publish();

    ERS_DEBUG(1, sout << "Done");
  }    

  /******************************************************/
  void afpCOBModule::resetHisto (std::string histoName)
  /******************************************************/
  {
    std::string sout = m_UID + "@afpCOBModule::resetHisto() ";
    ERS_LOG(sout<< " reset histo commad arrived to the RCD - histoName: "<<histoName);
  }

  /******************************************************/
  void afpCOBModule::resynch (const daq::rc::ResynchCmd &cmd)
  /******************************************************/
  {
    std::string sout = m_UID + "@afpCOBModule::resynch() ";
    ERS_LOG(sout<< " sent resynch to COB. Changing ECR on the fly. ");// Stopping RCD run to change ECR counter. " );
    //    m_RCDrunControl->StopRun();
   
    ERS_LOG(sout<< " with new ECR count: " << cmd.ecrCounts()+1 );
    //    m_RCDrunControl->Config();
    //    m_RCDrunControl->ResetL1id();
    m_RCDrunControl->SetECRpreset(cmd.ecrCounts()+1 ); //Note: only applied in the next RCD run (i.e. m_RCDrunControl->StartRun(); )
    //    m_RCDrunControl->StartRun();
    //    ERS_LOG(sout<< " started RCD run. "  );
    ERS_LOG(sout<< " continue RCD run. "  );

  }

  void afpCOBModule::disable        (const std::vector <std::string> &args){
    std::string sout = m_UID + "@afpCOBModule::disable ";
    ERS_LOG(sout << "Entered");
    m_doPublish=false;
    m_RCDrunControl->StopRun();
    m_RCDrunControl->UnConfig();
    ERS_LOG(sout << "Done"); 
  }
  
 /******************************************************/
 void afpCOBModule::onExit(daq::rc::FSM_STATE state) noexcept
 /******************************************************/
 {
   std::string sout = m_UID + "@afpCOBModule::onExit() ";
   ERS_LOG(sout << "Entered");
   m_doPublish=false;
   m_RCDrunControl->StopRun();
   m_RCDrunControl->UnConfig();
   // RCF::deinit();
   ERS_LOG(sout << "Done");
 }

  extern "C" 
  {
    extern ReadoutModule* createafpCOBModule ();
  }

  ReadoutModule *createafpCOBModule ()
  {
    return (new afpCOBModule ());
  }


}
