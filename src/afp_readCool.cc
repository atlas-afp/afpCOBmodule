#include <iostream>
#include <iomanip>
#include <cmdl/cmdargs.h>
#include <variant.hpp>
#include "afpCOBModule/AfpCool.hpp"

int main(int argc,char *argv[])
{
 
  CmdArgStr   timestamp ('u', "timestamp", "timestamp", "timestamp");
  CmdArgStr   dbConn('d',"dbConn","database-connection","string",CmdArg::isVALREQ );
  CmdArgStr   dbTag('t',"dbTag","database-tag","string",CmdArg::isVALREQ);
  CmdArgStr   jsonFile('j',"json","json-file","string",CmdArg::isVALREQ);
  CmdLine  cmd(*argv, &timestamp,&dbConn,&dbTag,&jsonFile,NULL);
  CmdArgvIter  arg_iter(--argc, ++argv);
  cmd.set(CmdLine::NO_ABORT|CmdLine::QUIET);
  cmd.parse(arg_iter);
  time_t ts;
if(timestamp) {
      std::tm t = {};
      std::istringstream ss((const char *) timestamp);
      ss >> std::get_time(&t, "%Y-%m-%d %H:%M:%S");
      if (ss.fail()) {
          std::cerr << "Parsing timestamp failed\n";
          return -1;
      } else {
          std::cout << std::put_time(&t, "%c") << '\n';
      }
      ts=std::mktime(&t);
  } else ts=std::time(NULL);
    variant32 v;
  try {
    std::string conn= (const char*)dbConn;
    std::string tag=  (const char*)dbTag;
    AfpReadCool cool(conn,tag,ts,v);
    //cool.read();
    cool.readSiT();
    cool.readToF();
    //cool.readRCE();
    std::ofstream jf((const char*)jsonFile);
    jf<<v;
  } catch(...) {
    std::cerr << "Cool read failed\n"; return -1;
  }


  return 0;
}
