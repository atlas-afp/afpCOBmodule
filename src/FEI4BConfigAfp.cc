#include "FEI4BConfigAfp.hh"
#include "PixelFEI4BConfig.hh"
#include "config/FEI4/FEI4BConfigFile.hh"
#include "config/AbsFormatter.hh"
#include "util/IblFeId.hh"

#include <boost/property_tree/ptree.hpp>
#include <iostream>


#include <RCF/RCF.hpp>
#include "RCFFEI4BAdapter.hh"
#include "util/RceName.hh"

#include <variant.hpp>

int FEI4BConfig::downloadConfig(int rce, int id){
  char binding[128];
  char rcename[128];
  sprintf(binding, "I_RCFFEI4BAdapter_%d", id);
  //sprintf(rcename, RCFHOST"%d", rce);
  sprintf(rcename, "ACM-AFP-ROD-0%d", rce);
  try {
    RcfClient<I_RCFFEI4BAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT), binding);
    client.RCFdownloadConfig( *m_config );
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error in downloadModuleConfig: "<<ex.getErrorString()<<std::endl;
  }
  return 0;
}


FEI4BConfig::FEI4BConfig(const variant32 &vconf): 
  //PixelConfig("FEI4B", filename, true, ipc::IPC_N_I4_PIXEL_COLUMNS, ipc::IPC_N_I4_PIXEL_ROWS, 1), 
  PixelConfig("FEI4B", std::string("tojestlipa"), true, ipc::IPC_N_I4_PIXEL_COLUMNS, ipc::IPC_N_I4_PIXEL_ROWS, 1), 
  m_config(new ipc::PixelFEI4BConfig){
  std::cout << "FEI4BConfig::FEI4BConfig - variant" << std::endl;
  FEI4BConfigFile fei4bfile;
       fei4bfile.fromVariant(vconf["config"], *m_config);
  m_name=(const char*)m_config->idStr;
  m_id=parseModuleId((const char*)m_config->idStr,m_config->FEGlobal.Chip_SN); 
  m_valid=true;
  m_hitbusfile=fei4bfile.getHitbusMaskFile();
  m_enablefile=fei4bfile.getEnableMaskFile();
}

FEI4BConfig::FEI4BConfig(std::string filename): 
  PixelConfig("FEI4B", filename, true, ipc::IPC_N_I4_PIXEL_COLUMNS, ipc::IPC_N_I4_PIXEL_ROWS, 1), 
  m_config(new ipc::PixelFEI4BConfig){
  FEI4BConfigFile fei4bfile;
       fei4bfile.readModuleConfig(m_config, filename);
  m_name=(const char*)m_config->idStr;
  m_id=parseModuleId((const char*)m_config->idStr,m_config->FEGlobal.Chip_SN); 
  m_valid=true;
  m_hitbusfile=fei4bfile.getHitbusMaskFile();
  m_enablefile=fei4bfile.getEnableMaskFile();
}

FECalib FEI4BConfig::getFECalib(int chip){
  FECalib fe;
  fe.cinjLo=m_config->FECalib.cinjLo;
  fe.cinjHi=m_config->FECalib.cinjHi;
  for(int i=0;i<4;i++) fe.vcalCoeff[i]=m_config->FECalib.vcalCoeff[i];
  fe.chargeCoeffClo=m_config->FECalib.chargeCoeffClo;
  fe.chargeCoeffChi=m_config->FECalib.chargeCoeffChi;
  fe.chargeOffsetClo=m_config->FECalib.chargeOffsetClo;
  fe.chargeOffsetChi=m_config->FECalib.chargeOffsetChi;
  fe.monleakCoeff=m_config->FECalib.monleakCoeff;
  return fe;
}
void FEI4BConfig::setFECalib(int chip, FECalib fe){
  m_config->FECalib.cinjLo=fe.cinjLo;
  m_config->FECalib.cinjHi=fe.cinjHi;
  for(int i=0;i<4;i++) m_config->FECalib.vcalCoeff[i]=fe.vcalCoeff[i];
  m_config->FECalib.chargeCoeffClo=fe.chargeCoeffClo;
  m_config->FECalib.chargeCoeffChi=fe.chargeCoeffChi;
  m_config->FECalib.chargeOffsetClo=fe.chargeOffsetClo;
  m_config->FECalib.chargeOffsetChi=fe.chargeOffsetChi;
  m_config->FECalib.monleakCoeff=fe.monleakCoeff;
}
unsigned FEI4BConfig::getThresholdDac(int chip, int col, int row){
  return m_config->FETrims.dacThresholdTrim[col][row];
}
void FEI4BConfig::setThresholdDac(int chip, int col, int row, int val){
  m_config->FETrims.dacThresholdTrim[col][row]=val;
}
unsigned FEI4BConfig::getFeedbackDac(int chip, int col, int row){
  return m_config->FETrims.dacFeedbackTrim[col][row];
}
void FEI4BConfig::setFeedbackDac(int chip, int col, int row, int val){
  m_config->FETrims.dacFeedbackTrim[col][row]=val;
}
unsigned FEI4BConfig::getIf(int chip){
  return m_config->FEGlobal.PrmpVbpf;
}
void FEI4BConfig::setIf(int chip, int val){
  m_config->FEGlobal.PrmpVbpf=val;
}
unsigned FEI4BConfig::getGDac(int chip){
  return m_config->FEGlobal.Vthin_AltFine;
}
void FEI4BConfig::setGDac(int chip, int val){
  m_config->FEGlobal.Vthin_AltFine=val;
}
void FEI4BConfig::setGDacCoarse(int chip, int val){
  m_config->FEGlobal.Vthin_AltCoarse=val;
}
unsigned FEI4BConfig::getFEMask(int chip, int col, int row){
  return m_config->FEMasks[col][row];
}
void FEI4BConfig::setFEMask(int chip, int col, int row, int val){
  m_config->FEMasks[col][row] = val;
}
void FEI4BConfig::writeModuleConfig(const std::string &base, const std::string &confdir, const std::string &configname, const std::string &key){
  FEI4BConfigFile cf;
  cf.writeModuleConfig(m_config, base, confdir, configname, key);
}
void FEI4BConfig::configureFormatter(AbsFormatter* formatter){
  boost::property_tree::ptree *pt=new boost::property_tree::ptree;
  pt->put("HitDiscCnfg",m_config->FEGlobal.HitDiscCnfg);
  formatter->configure(pt);
  delete pt;
}
